﻿using UnityEngine;
using UnityEditor;

namespace Sense3d.FileLoading
{
    public class ReadOnlyAttribute : PropertyAttribute { }

}
