
using Sense3d.Fileloading.Utils;
using UnityEditor;
using UnityEngine;


namespace Sense3d.Fileloading
{
    [CustomEditor(typeof(AppUrlConfig))]
    public class AppConfigInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();//Draw inspector UI of ImageEditor

            AppUrlConfig componentTarget = (AppUrlConfig)target;

            if (GUILayout.Button("Reload Appconfig"))
            {
                EditorHelpers.LoadAppConfig(componentTarget);
            }

        }

    }
}
