using Sense3d.Fileloading.RemoteHandlers;
using UnityEditor;
using UnityEngine;

namespace Sense3d.Fileloading.UI

{
    [CustomEditor(typeof(RemoteVideoUI))]
    public class RemoteVideoUIInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            
            RemoteVideoUI targetComponent = (RemoteVideoUI)target;
            /*
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Placeholder");
            targetComponent.remoteFileHandler = (RemoteVideoHandler)EditorGUILayout.ObjectField(targetComponent.remoteFileHandler, typeof(RemoteVideoHandler), false);
            EditorGUILayout.EndHorizontal();
            */
            DrawDefaultInspector();
            if (targetComponent.remoteFileHandler != null)
            {
                if (GUILayout.Button("Show available content"))
                {
                    targetComponent.remoteFileHandler.LoadLocalContent();
                    targetComponent.videoPlayer.url = targetComponent.remoteFileHandler.content;
                    //targetComponent.sprite = targetComponent.remoteFileHandler.GetAvailableContent();
                }

                if (GUILayout.Button("Try DownloadFile and Update"))
                {
                    targetComponent.remoteFileHandler.DownloadRemoteFile();
                }
            }

           // base.OnInspectorGUI();


        }

    }
}
