using Sense3d.Fileloading.RemoteHandlers;
using UnityEditor;
using UnityEngine;

namespace Sense3d.Fileloading.UI

{
    [CustomEditor(typeof(RemoteImageUI))]
    public class RemoteImageUIInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            RemoteImageUI targetComponent = (RemoteImageUI)target;

            /*
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("RemoteImageHandler");
            targetComponent.remoteFileHandler = (RemoteImageHandler)EditorGUILayout.ObjectField(targetComponent.remoteFileHandler, typeof(RemoteImageHandler), false);
            EditorGUILayout.EndHorizontal();
            */
            DrawDefaultInspector();
            if (targetComponent.remoteFileHandler != null)
            {
                if (GUILayout.Button("Show available content"))
                {
                    targetComponent.remoteFileHandler.LoadLocalContent();
                    targetComponent.imageField.sprite = targetComponent.remoteFileHandler.GetAvailableContent();
                }

                if (GUILayout.Button("Try DownloadFile and Update"))
                {
                    targetComponent.remoteFileHandler.DownloadRemoteFile();
                }
            }

            //base.OnInspectorGUI();

            /*
                         RemoteImageHandler remoteFileHandler = (RemoteImageHandler)target;
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Placeholder");
            remoteFileHandler.placeholder = (Sprite)EditorGUILayout.ObjectField(remoteFileHandler.placeholder, typeof(Sprite), false);
            EditorGUILayout.EndHorizontal();
             
             */

        }

    }
}
