using Sense3d.Fileloading.RemoteHandlers;
using UnityEditor;
using UnityEngine;

namespace Sense3d.Fileloading.UI

{
    [CustomEditor(typeof(RemoteAudioUI))]
    public class RemoteAudioUIInspector : Editor
    {
        public override void OnInspectorGUI()
        {
           
            RemoteAudioUI targetComponent = (RemoteAudioUI)target;
            /*
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Placeholder");
            targetComponent.remoteFileHandler = (RemoteAudioHandler)EditorGUILayout.ObjectField(targetComponent.remoteFileHandler, typeof(RemoteAudioHandler), false);
            EditorGUILayout.EndHorizontal();*/
            DrawDefaultInspector();
            if (targetComponent.remoteFileHandler != null)
            {
                if (GUILayout.Button("Show available content"))
                {
                    targetComponent.remoteFileHandler.LoadLocalContent();
                    targetComponent.audioSource.clip = targetComponent.remoteFileHandler.content;
                    //targetComponent.sprite = targetComponent.remoteFileHandler.GetAvailableContent();
                }

                if (GUILayout.Button("Try DownloadFile and Update"))
                {
                    targetComponent.remoteFileHandler.DownloadRemoteFile();
                }
            }

           // base.OnInspectorGUI();


        }

    }
}
