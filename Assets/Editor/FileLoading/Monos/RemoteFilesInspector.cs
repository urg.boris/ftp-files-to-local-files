﻿using UnityEditor;
using UnityEngine;

namespace Sense3d.Fileloading.Monos

{
    
    [CustomEditor(typeof(RemoteFilesMono), true)]
    public class RemoteFilesMonoInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            RemoteFilesMono rootOfremotes = (RemoteFilesMono)target;
            if (GUILayout.Button("Update all paths"))
            {
                rootOfremotes.UpdateAllPaths();
            }
            if (GUILayout.Button("Download all remote files"))
            {
                rootOfremotes.DownloadAllRemoteFiles();
            }
            base.OnInspectorGUI();
        }
        

    }
}
