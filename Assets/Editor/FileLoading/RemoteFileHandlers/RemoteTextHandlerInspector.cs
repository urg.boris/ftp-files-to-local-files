using UnityEditor;
using UnityEngine;

namespace Sense3d.Fileloading.RemoteHandlers

{
    [CustomEditor(typeof(RemoteTextHandler))]
    public class RemoteTextHandlerInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();//Draw inspector UI of ImageEditor

            RemoteTextHandler remoteFileHandler = (RemoteTextHandler)target;
            if (GUILayout.Button("Update current content"))
            {
                remoteFileHandler.LoadLocalContent();
            }
            if (GUILayout.Button("Try DownloadFile"))
            {
                remoteFileHandler.DownloadRemoteFile();
            }

        }

    }
}
