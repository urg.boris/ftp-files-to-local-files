using UnityEditor;
using UnityEngine;

namespace Sense3d.Fileloading.RemoteHandlers

{
    [CustomEditor(typeof(RemoteGalleryHandler))]
    public class RemoteGalleryHandlerInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();//Draw inspector UI of ImageEditor

            RemoteGalleryHandler remoteFileHandler = (RemoteGalleryHandler)target;
            if (GUILayout.Button("Update current content"))
            {
                remoteFileHandler.LoadLocalContent();
            }
            if (GUILayout.Button("DownloadFile and load"))
            {
                remoteFileHandler.DownloadRemoteFileFromInspector();
            }
        }

    }
}
