using UnityEditor;
using UnityEngine;

namespace Sense3d.Fileloading.RemoteHandlers

{
    [CustomEditor(typeof(RemoteImageHandler))]
    public class RemoteImageHandlerInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();//Draw inspector UI of ImageEditor

            RemoteImageHandler remoteFileHandler = (RemoteImageHandler)target;
            if (GUILayout.Button("Update current content"))
            {
                remoteFileHandler.LoadLocalContent();
            }
            if (GUILayout.Button("Try DownloadFile"))
            {
                remoteFileHandler.DownloadRemoteFile();
            }
        }

    }
}
