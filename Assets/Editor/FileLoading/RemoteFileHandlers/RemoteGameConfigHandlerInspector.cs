using UnityEditor;
using UnityEngine;

namespace Sense3d.Fileloading.RemoteHandlers

{
    [CustomEditor(typeof(RemoteGameConfigHandler))]
    public class RemoteGameConfigHandlerInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();//Draw inspector UI of ImageEditor

            RemoteGameConfigHandler remoteFileHandler = (RemoteGameConfigHandler)target;
            if (GUILayout.Button("Update current content"))
            {
                remoteFileHandler.LoadLocalContent();
            }
            if (GUILayout.Button("Try DownloadFile"))
            {
                remoteFileHandler.DownloadRemoteFile();
            }

        }

    }
}
