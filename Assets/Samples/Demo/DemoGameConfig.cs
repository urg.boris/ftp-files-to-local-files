﻿using System.Collections;
using System.Collections.Generic;
using Sense3d.Fileloading;
using Sense3d.Fileloading.RemoteHandlers;
using UnityEngine;

namespace Sense3d.Fileloading.Demo
{
    [CreateAssetMenu(fileName = "DemoGameConfig", menuName = "Internal/DemoGameConfig")]
    public class DemoGameConfig : GameConfig
    {

        
    }
}
