﻿using Sense3d.Fileloading.RemoteHandlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sense3d.Fileloading
{
    public class LinkFileNameToRemoteFile
    {
        public string fileName;
        public RemoteFileHandler remoteFile;
    }
    public class LinkRemoteFileTop
    {
        public string fileName;
        public RemoteFileHandler remoteFile;
    }
}
