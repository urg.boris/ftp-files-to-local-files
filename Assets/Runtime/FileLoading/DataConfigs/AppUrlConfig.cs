using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sense3d.Fileloading.Utils;

namespace Sense3d.Fileloading
{
    [CreateAssetMenu(fileName = "UrlAppConfig", menuName = "Internal/UrlAppConfig")]
    public class AppUrlConfig : ScriptableObject
    {
        //public string savePath;
        public string urlPath;
        //public string appConfigRelativeDir;

        private void Awake()
        {
#if UNITY_EDITOR
            EditorHelpers.LoadAppConfig<AppUrlConfig>(this);
#endif
        }
    }
}

