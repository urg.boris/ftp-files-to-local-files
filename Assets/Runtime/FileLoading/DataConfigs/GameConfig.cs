using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sense3d.Fileloading
{
    public abstract class GameConfig : ScriptableObject
    {
        public string id;
        public string dirName;
        public string GetSubDirPath(string fileName)
        {
           // Debug.Log("GetSubDirPath"+ fileName+"->"+ dirName + "/" + fileName);
            return dirName + "/" + fileName;
        }
    }
}
