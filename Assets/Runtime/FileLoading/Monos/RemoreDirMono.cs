﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Sense3d.Fileloading.Monos
{
    public class RemoreDirMono : ARemoteMono
    {
        public string dirName;
        [SerializeField]
        public IRemotePathStructure[] files;
        /*
        public void GetRemoteChildren()
        {
            files = transform.GetComponentsInChildren<IRemotePathStructure>();
            Debug.Log(name+": files=" + files.Length);
        }*/

        public override void LoadStructure()
        {
            dirName = name ;
            LoadBaseStructure();
            var children = transform.GetComponentsInChildren<IRemotePathStructure>();
            //!! skip first element
            files =  new IRemotePathStructure[children.Length - 1];
            for (int i = 0; i < children.Length-1; i++)
            {
                files[i] = children[i + 1];
                files[i].LoadStructure();
            }
           // Debug.Log(name + ": files=" + files.Length);
        }

        public override void AddjustUrlPath()
        {
            urlPath = name ;
            foreach (var item in files)
            {
                item.AddjustUrlPath();
            }
            
        }
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public override string GetPathName()
        {
            return dirName;
        }
    }
}
