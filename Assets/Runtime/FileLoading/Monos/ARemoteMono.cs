﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
namespace Sense3d.Fileloading.Monos
{
    public interface IRemotePathStructure
    {
        void LoadStructure();
        void AddjustUrlPath();
        string GetPath();
        IRemotePathStructure GetParent();
        string GetPathName();
        // void DownloadContent()
    }
    public abstract class ARemoteMono : MonoBehaviour, IRemotePathStructure
    {
        public IRemotePathStructure parent;

        [HideInInspector]
        public string urlPath;


        public void LoadBaseStructure()
        {
            parent = GetParent();
        }


        public string GetPath()
        {
            if (this.GetParent() == null)
            {
                return this.GetPathName();
            }
            return this.GetParent().GetPath() + "/" + this.GetPathName();
        }

        public IRemotePathStructure GetParent()
        {
            return  transform.parent.GetComponent<IRemotePathStructure>();
        }

        public abstract string GetPathName();

        // Start is called before the first frame update
        void Start()
        {

        }

        public abstract void AddjustUrlPath();

        public abstract void LoadStructure();
    }
}
