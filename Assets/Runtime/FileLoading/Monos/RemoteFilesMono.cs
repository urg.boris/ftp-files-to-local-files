﻿using Sense3d.Fileloading.RemoteHandlers;
//using Sense3d.Ipn.Common;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Sense3d.Fileloading.Utils;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Sense3d.Fileloading.Monos
{
    public abstract class RemoteFilesMono: MonoBehaviour
    {

        public List<Task<(bool, string)>> downloadTasks;
        public List<Task<bool>> loadTasks;

        public List<LinkFileNameToRemoteFile> linksFileNameToRemoteFile;

        public GameObject rootDir;
        public RemoteGameConfigFileMono remoteGameConfigMono;
        public AppUrlConfig appUrlConfig;
        public GameConfig gameConfigBase; 

        // [HideInInspector]
        public string rootUrl;
        
        public RemoteFileHandler[] remoteFileHandlers;
/*
        public void InjectBase(GameConfig config)
        {
            //Debug.Log("injecting sub uper class");
            this.gameConfigBase = config;
            //this.remoteFilesBase = remoteFiles;
        }*/

        public void SetFileHandler(RemoteFileHandler[] remoteFileHandlers)
        {
            this.remoteFileHandlers = remoteFileHandlers;
        }

        public abstract void PrepareDownloadTasks();
        public abstract void PrepareLoadTasks();
        public abstract void AfterLoadTasks();
        public abstract void UpdateRemoteFileLinksFromLoadedConfig();
        public abstract void SetGameConfig(string json);

        public virtual async void PrepareContent()
        {
            UpdateAllPaths();
            PrepareLoadTasks();
            await LoadAllLocalFiles();


        }

        public virtual void LoadStructure()
        {
           // BeforeLoadingStructure();
            var rootStruct = rootDir.GetComponent<IRemotePathStructure>();
            
            if (rootStruct!=null)
            {
              //  Debug.Log("------------------------------------------");
                rootUrl = rootDir.name + "/";
                gameConfigBase.dirName = rootDir.name;
                rootStruct.LoadStructure();
                rootStruct.AddjustUrlPath();
            }
        }

        public void ClearConsole()
        {
#if UNITY_EDITOR
            var assembly = Assembly.GetAssembly(typeof(UnityEditor.Editor));
            var type = assembly.GetType("UnityEditor.LogEntries");
            var method = type.GetMethod("Clear");
            method.Invoke(new object(), null);
#endif
        }

        public async void UpdateAllPaths()
        {

            //TODO sjednotis DownloadAllRemoteFiles
            ClearConsole();
            await GetOrCreateGameConfig();
            UpdateRemoteFileLinksFromLoadedConfig();
            
            LoadStructure();
        }

        protected void AddDownloadTask(Task<(bool, string)> task, string url)
        {
            DownloagDebugLog("starting download: " + url);
            downloadTasks.Add(task);
        }

        public async void DownloadAllRemoteFiles()
        {
       
            ClearConsole();
            DownloagDebugLog("-----------Download starting");
           // commonUI.StartDownloading();

            await DownloadGameConfig();

            UpdateRemoteFileLinksFromLoadedConfig();
            LoadStructure();
          //
           

            PrepareDownloadTasks();
            

            (bool, string)[] results = await Task.WhenAll(downloadTasks);


            foreach ((bool, string) task in results)
            {
                DownloadDebugLogFile(task.Item1, task.Item2);
            }

            DownloagDebugLog("-----------Download finnished");
           // commonUI.DownloadingFinnished();
        }

        public void DownloagDebugLog(string msg)
        {
            if (Application.isPlaying)
            {
                //commonUI.AppendLog(msg);
            }
            Debug.Log(msg);
        }

        public void DownloadDebugLogFile(bool success, string filePath)
        {
            if (success)
            {
                DownloagDebugLog("ok: " + filePath);
            }
            else
            {
                DownloagDebugLog("fail: " + filePath);
            }
        }

        public async Task<bool> GetOrCreateGameConfig(bool loadFromServer = false)
        {
            if (loadFromServer)
            {
                await DownloadGameConfig();
            }
            else
            {
                if (!remoteGameConfigMono.remoteFileHandler.ExistsFileLocally())
                {
                    await DownloadGameConfig();
                }
                else
                {
                    await LoadGameConfig();
                }
            }
            return true;
        }


        protected void LoadAppUrlConfig()
        {
            var appConfigLoader = new JsonConfigFileHandler(Enviroment.GetConfigsPath());
            appConfigLoader.LoadConfig(appUrlConfig, Enviroment.APP_CONFIG_FILENAME);
            Enviroment.AppUrlConfig = appUrlConfig;
        }

        protected async Task<bool> LoadLocalGameConfigContent()
        {
            await remoteGameConfigMono.remoteFileHandler.LoadLocalContentBlocking();
            // Debug.Log(remoteFiles.remoteGameConfigMono.content);
            SetGameConfig(((RemoteGameConfigHandler)remoteGameConfigMono.remoteFileHandler).content);
            return true;
        }   
        protected async Task<bool> DownloadGameConfig() {
            LoadAppUrlConfig();


            //TODS urlks
            //remoteConfigHandler.relativeUrl = remoteConfigHandler.relativeUrl + FileLoading.REMOTE_FILENAME;
            //must await main config
            //  DownloagDebugLog("starting download: " + remoteFilesBase.remoteGameConfigMono.relativeUrl);
            (bool, string) configDownloaded = await remoteGameConfigMono.remoteFileHandler.DownloadTask();
            if (!configDownloaded.Item1)
            {
                Debug.LogError("Cant reach " + remoteGameConfigMono.remoteFileHandler.GetUrlPath());
                // commonUI.ShowDownloadText("Cant reach " + remoteFilesBase.remoteGameConfigMono.GetUrlPath());
                return false;
            }
            else
            {
                Debug.Log("succes load of game config");
                //Debug.Log(configDownloaded.Item1);
                Debug.Log(configDownloaded.Item2);
            }

            await LoadLocalGameConfigContent();
            //JsonUtility.FromJsonOverwrite(remoteGameConfigMono.content, gameConfig);
            //SaveScritpableObjectParams(remoteFilesBase.remoteGameConfigMono);
            return true;
        }

        protected async Task<bool> LoadGameConfig()
        {
            LoadAppUrlConfig();
            
            await LoadLocalGameConfigContent();

            return true;
        }


        public async Task<bool> LoadAllLocalFiles()
        {
            DownloagDebugLog("-----------Local load starting");

            await GetOrCreateGameConfig();
            DownloagDebugLog("-----------Game config loaded");
            UpdateRemoteFileLinksFromLoadedConfig();

            PrepareLoadTasks();


            await Task.WhenAll(loadTasks);
            AfterLoadTasks();
            DownloagDebugLog("-----------Local load finnished");
            return true;
        }

        public async Task<bool> LoadImageToUI(RemoteImageHandler remoteImage, RawImage imageUI, bool setNaitiveSize = false)
        {
            await remoteImage.LoadLocalContentBlocking();
            if (remoteImage.content != null)
            {
                imageUI.texture = remoteImage.content.texture;
                if (setNaitiveSize)
                {
                    imageUI.SetNativeSize();
                }
            }
            return true;
        }

        public async Task<bool> LoadImagesToButton(RemoteImageHandler activeImage, RemoteImageHandler inActiveImage, Button button, Vector3 position)
        {
            await activeImage.LoadLocalContentBlocking();
            await inActiveImage.LoadLocalContentBlocking();

            SpriteState spriteState = new SpriteState();
            if (activeImage.content != null)
            {
                spriteState.pressedSprite = activeImage.content;
            }
            if (inActiveImage.content != null)
            {
                button.image.sprite = inActiveImage.content;
                button.image.SetNativeSize();
            }
            button.spriteState = spriteState;
            button.GetComponent<RectTransform>().anchoredPosition = position;
            return true;
        }
        public static void SaveScritpableObjectParams(ScriptableObject scriptableObject)
        {
#if UNITY_EDITOR
            EditorUtility.SetDirty(scriptableObject);
#endif
        }

    }
}
