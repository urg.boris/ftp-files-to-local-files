﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using Sense3d.Fileloading.RemoteHandlers;
using UnityEngine.Networking;
using System.Threading.Tasks;

namespace Sense3d.Fileloading.RemoteHandlers
{
    [CreateAssetMenu(fileName = "RemoteAudio", menuName = "RemoteFileHandlers/Audio")]
    public class RemoteAudioHandler : RemoteFileHandler
    {
        const AudioType AUDIO_TYPE = AudioType.WAV;// AudioType.MPEG;//AudioType.OGGVORBIS;//

        public AudioClip placeholder;

        [HideInInspector]
        public AudioClip content;

        /*
        private void Awake()
        {
            GetContent();
        }*/



        public override async void DownloadRemoteFile()
        {
            await DownloadTask();

            LoadLocalContent();

        }
        /*
        private async void Awake()
        {
            if (ExistsFileLocally())
            {
                return
            }
        }*/

        public AudioClip GetAvailableContent()
        {
            return content;
        }

        public override async void LoadLocalContent()
        {
            if (ExistsFileLocally())
            {
                content = await LoadLocallyTask();
            }
            else
            {
                content = placeholder;
                FileLoadedWithPlaceholder(GetSavePath());
            }
        }

        public  override async Task<bool> LoadLocalContentBlocking()
        {
            if (ExistsFileLocally())
            {
                content = await LoadLocallyTask();
            }
            else
            {
                content = placeholder;
                FileLoadedWithPlaceholder(GetSavePath());
            }
            return true;
        }


        public override async Task<(bool, string)> DownloadTask()
        {
            var urlPath = GetUrlPath();
            if (fileName == "")
            {
                return (true, "skipped:" + name);
            }
            //Debug.Log(name);
            FileDownloadStarted(urlPath);
            UnityWebRequest www = UnityWebRequest.Get(urlPath);
            www.disposeDownloadHandlerOnDispose = false;
            UnityWebRequestAsyncOperation request = www.SendWebRequest();
            while (!request.isDone || !www.isDone) await Task.Delay(30);

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.LogError($"WWW ({www.url}) has error: {www.error}");
                return (false, urlPath);
            }
           // Debug.Log(www.downloadHandler.data);

            //AudioClip returnObject = DownloadHandlerAudioClip.GetContent(www);


            string savePath = GetSavePath();
            CreateDirsForFilePath(savePath);
            //Debug.Log("www.downloadedBytes=" + www.downloadedBytes);
            System.IO.File.WriteAllBytes(savePath, www.downloadHandler.data);
            //FileDownloaded(urlPath);

            return (true, urlPath);
            /*
            var urlPath = GetUrlPath();
            UnityWebRequest www = UnityWebRequestMultimedia.GetAudioClip(urlPath, AUDIO_TYPE);
            www.disposeDownloadHandlerOnDispose = false;
            UnityWebRequestAsyncOperation request = www.SendWebRequest();
            while (!request.isDone || !www.isDone) await Task.Delay(30);

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.LogError($"WWW ({www.url}) has error: {www.error}");
                return false;
            }
            Debug.Log(DownloadHandlerAudioClip.GetContent(www).length);

            //AudioClip returnObject = DownloadHandlerAudioClip.GetContent(www);
          

            string savePath = GetSavePath();
            System.IO.File.WriteAllBytes(savePath, ((DownloadHandlerAudioClip)www.downloadHandler).data);
            FileDownloaded(urlPath);
                
            return true;*/



            /*
            using (UnityWebRequest www = UnityWebRequestMultimedia.GetAudioClip(urlPath, AUDIO_TYPE))
            {
                UnityWebRequestAsyncOperation request = www.SendWebRequest();
                while (!request.isDone || !www.isDone) await Task.Delay(30);

                if (www.result != UnityWebRequest.Result.Success)
                {
                    Debug.LogError($"WWW ({urlPath}) has error: {www.error}");
                    return null;
                }

                AudioClip returnObject = DownloadHandlerAudioClip.GetContent(www);


                string savePath = GetSavePath();
                System.IO.File.WriteAllBytes(savePath, www.downloadHandler.data);

                FileDownloaded(urlPath);

                return returnObject;
            }*/

        }

        public async Task<AudioClip> LoadLocallyTask()
        {
            var urlPath = GetSavePathWithProtocol();

            UnityWebRequest www = UnityWebRequestMultimedia.GetAudioClip(urlPath, AUDIO_TYPE);
            await HandleWWWRequest(www);
            AudioClip returnObject = DownloadHandlerAudioClip.GetContent(www);
            returnObject.name = relativeUrl;
            FileLoadedLocaly(urlPath);
            return returnObject;
/*
            using (UnityWebRequest www = UnityWebRequestMultimedia.GetAudioClip(urlPath, AUDIO_TYPE))
            {
                UnityWebRequestAsyncOperation request = www.SendWebRequest();
                while (!request.isDone || !www.isDone) await Task.Delay(30);

                if (www.result != UnityWebRequest.Result.Success)
                {
                    Debug.LogError($"WWW ({urlPath}) has error: {www.error}");
                    return null;
                }

                FileLoadedLocaly(urlPath);

                AudioClip returnObject = DownloadHandlerAudioClip.GetContent(www);

                return returnObject;
            }*/
        }

        public override void DisposeContent()
        {
            AudioClip.Destroy(content);
        }


    }
}
