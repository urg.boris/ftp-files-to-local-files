﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading.Tasks;
using System.IO;
using UnityEngine.Networking;

namespace Sense3d.Fileloading.RemoteHandlers
{
    [CreateAssetMenu(fileName = "RemoteImage", menuName = "RemoteFileHandlers/Image")]
    public class RemoteImageHandler : RemoteFileHandler
    {
        public Sprite placeholder;

        
        public Sprite content;

        /*
        private void Awake()
        {
            GetContent();
        }*/


        public  override async void DownloadRemoteFile()
        {
            await DownloadTask();

            LoadLocalContent();

        }
        /*
        private async void Awake()
        {
            if (ExistsFileLocally())
            {
                return
            }
        }*/

        public Sprite GetAvailableContent()
        {
            return content;
        }

        public override async void LoadLocalContent()
        {
            if (ExistsFileLocally())
            {
                Texture2D texture = await LoadLocallyTask();
                content =  Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
            }
            else
            {
                content = placeholder;
                FileLoadedWithPlaceholder(GetSavePath());
            }
        }

        public override async Task<bool> LoadLocalContentBlocking()
        {
            if (ExistsFileLocally())
            {
                Texture2D texture = await LoadLocallyTask();
                content = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
            }
            else
            {
                content = placeholder;
                FileLoadedWithPlaceholder(GetSavePath());

            }
            return true;
        }



        public override async Task<(bool, string)> DownloadTask()
        {
            var urlPath = GetUrlPath();
            if (fileName == "")
            {
                return (true, "skipped:" + name);
            }
            FileDownloadStarted(urlPath);
            UnityWebRequest www = UnityWebRequestTexture.GetTexture(urlPath);
            bool resultOk = await HandleWWWRequest(www) != null;
            if (resultOk)
            {
                Texture2D texture = DownloadHandlerTexture.GetContent(www);

                var bytes = texture.EncodeToPNG();
                string savePath = GetSavePath();
                CreateDirsForFilePath(savePath);
                System.IO.File.WriteAllBytes(savePath, bytes);

                //FileDownloaded(urlPath);
            }
            return (resultOk, urlPath);

            /*
            using (UnityWebRequest www = UnityWebRequestTexture.GetTexture(filePath))
            {
                UnityWebRequestAsyncOperation request = www.SendWebRequest();
                while (!request.isDone || !www.isDone) await Task.Delay(30);

                if (www.result != UnityWebRequest.Result.Success)
                {
                    Debug.LogError($"WWW ({filePath}) has error: {www.error}");
                    return null;
                }

                
                Texture2D texture = DownloadHandlerTexture.GetContent(www);

                string savePath = GetSavePath();
                System.IO.File.WriteAllBytes(savePath, www.downloadHandler.data);

                FileDownloaded(filePath);

                return texture;
            }*/

        }

        public async Task<Texture2D> LoadLocallyTask()
        {
            string savePath = GetSavePathWithProtocol();
           // Debug.Log("get save");
          //  Debug.Log(savePath);
            UnityWebRequest www = UnityWebRequestTexture.GetTexture(savePath);
            await HandleWWWRequest(www);

            FileLoadedLocaly(savePath);
            return DownloadHandlerTexture.GetContent(www);
            /*
            using (UnityWebRequest www = UnityWebRequestTexture.GetTexture(savePath))
            {
                UnityWebRequestAsyncOperation request = www.SendWebRequest();
                while (!request.isDone || !www.isDone) await Task.Delay(30);

                if (www.result != UnityWebRequest.Result.Success)
                {
                    Debug.LogError($"WWW ({savePath}) has error: {www.error}");
                    return null;
                }

                FileLoadedLocaly(savePath);

                return DownloadHandlerTexture.GetContent(www);
            }*/
        }

        public override void DisposeContent()
        {
            Sprite.Destroy(content);
        }
    }
}