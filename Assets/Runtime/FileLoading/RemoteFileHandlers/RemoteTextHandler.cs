﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using Sense3d.Fileloading.RemoteHandlers;
using UnityEngine.Networking;
using System.Threading.Tasks;

namespace Sense3d.Fileloading
{
    [CreateAssetMenu(fileName = "RemoteText", menuName = "RemoteFileHandlers/Text")]
    public class RemoteTextHandler : RemoteFileHandler
    {
        public string placeholder;

       //[HideInInspector]
        public string content;

        /*
        private void Awake()
        {
            GetContent();
        }*/


        public override  async void DownloadRemoteFile()
        {
            await DownloadTask();

            LoadLocalContent();

        }
        /*
        private async void Awake()
        {
            if (ExistsFileLocally())
            {
                return
            }
        }*/

        public string GetAvailableContent()
        {
            return content;
        }

        public override async void LoadLocalContent()
        {
            if (ExistsFileLocally())
            {
                content = await LoadLocallyTask();
            }
            else
            {
                content = placeholder;
                FileLoadedWithPlaceholder(GetSavePath());
            }
        }

        public override async Task<bool> LoadLocalContentBlocking()
        {
            if (ExistsFileLocally())
            {
                content = await LoadLocallyTask();
            }
            else
            {
                content = placeholder;
                FileLoadedWithPlaceholder(GetSavePath());
            }
            return true;
        }


        public override async Task<(bool, string)> DownloadTask()
        {
            var urlPath = GetUrlPath();
            Debug.Log(urlPath);
            if (fileName == "")
            {
                return (true, "skipped:" + name);
            }
            FileDownloadStarted(urlPath);
            UnityWebRequest www = UnityWebRequest.Get(urlPath);
            bool resultOk = await HandleWWWRequest(www) != null;
            if (resultOk)
            {
                string savePath = GetSavePath();
                CreateDirsForFilePath(savePath);
                System.IO.File.WriteAllText(savePath, www.downloadHandler.text);
               // content = www.downloadHandler.text;
                //FileDownloaded(urlPath);
            }
            return (resultOk, urlPath);


            /*
            using (UnityWebRequest www = UnityWebRequest.Get(urlPath))
            {
                UnityWebRequestAsyncOperation request = www.SendWebRequest();
                while (!request.isDone || !www.isDone) await Task.Delay(30);

                if (www.result != UnityWebRequest.Result.Success)
                {
                    Debug.LogError($"WWW ({urlPath}) has error: {www.error}");
                    return null;
                }
                string savePath = GetSavePath();
                System.IO.File.WriteAllText(savePath, www.downloadHandler.text);

                FileDownloaded(urlPath);
                return www.downloadHandler.text;// request.webRequest.downloadHandler.text;
            }*/
        }

        public async Task<string> LoadLocallyTask()
        {
            string savePath = GetSavePathWithProtocol();
            UnityWebRequest www = UnityWebRequest.Get(savePath);
            await HandleWWWRequest(www);
            FileLoadedLocaly(savePath);

            return www.downloadHandler.text;

            /*
            using (UnityWebRequest www = UnityWebRequest.Get(savePath))
            {
                UnityWebRequestAsyncOperation request = www.SendWebRequest();
                while (!request.isDone || !www.isDone) await Task.Delay(30);

                if (www.result != UnityWebRequest.Result.Success)
                {
                    Debug.LogError($"WWW ({savePath}) has error: {www.error}");
                    return null;
                }

                FileLoadedLocaly(savePath);

                return www.downloadHandler.text;// request.webRequest.downloadHandler.text;
            }*/
        }

        public override void DisposeContent()
        {
            content = "";
        }
    }
}
