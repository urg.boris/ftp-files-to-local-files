﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading.Tasks;
using System.IO;
using UnityEngine.Networking;
using Sense3d.Fileloading.DataConfigs;
using Sense3d.Fileloading.Utils;

namespace Sense3d.Fileloading.RemoteHandlers
{
    [CreateAssetMenu(fileName = "RemoteGallery", menuName = "RemoteFileHandlers/Gallery")]
    public class RemoteGalleryHandler : RemoteFileHandler
    {
        const string GALLERY_CONFIG_FILENAME = "gallery.json";
        const string GET_DIR_SLIDES_FILENAME = "get-dir-slides.php";

        public Sprite placeholder;

        [HideInInspector]
        public Texture2D[] content;

        string urlDirPath;
        string urlConfigFilePath;
        string dirSavePath;
        string configSavePath;

        public override bool ExistsFileLocally()
        {

            //Debug.Log(Enviroment.AppConfig);
            var filepath = GetGalleryConfigSavePath();
            //Debug.Log("ee localy="+ filepath);
            return File.Exists(filepath);
        }

        public override async void DownloadRemoteFile()
        {
            await DownloadTask();

            // LoadLocalContent();

        }

        public async void DownloadRemoteFileFromInspector()
        {
            await DownloadTask();
            Debug.Log("all downloaded");

            // LoadLocalContent();

        }
        /*
        private async void Awake()
        {
            if (ExistsFileLocally())
            {
                return
            }
        }*/

        public Texture2D[] GetAvailableContent()
        {
            if (content!= null && content.Length > 0)
            {
                return content;
            }
            else
            {
                return GetMimimunContent();
            }

        }

        public Texture2D[] GetMimimunContent()
        {
            return new Texture2D[1] { (placeholder!=null && placeholder.texture) ? placeholder.texture : new Texture2D(2,2) };
        }

        public override Task<bool> LoadLocalContentBlocking()
        {
            throw new NotImplementedException();
        }

        public override async void LoadLocalContent()
        {
            if (ExistsFileLocally())
            {
                Texture2D[] textures = await LoadLocallyTask();
                var sprites = new Sprite[textures.Length];
              /*  for (int i = 0; i < textures.Length; i++)
                {
                    sprites[i] = Sprite.Create(textures[i], new Rect(0.0f, 0.0f, textures[i].width, textures[i].height), new Vector2(0.5f, 0.5f), 100.0f);
                }*/
                content = textures;
                //content =  Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
            }
            else
            {

                content = GetMimimunContent();
                FileLoadedWithPlaceholder(GetSavePath());
            }
        }



        public string GetGalleryFileUrlPath(string fileName)
        {
            return Path.Combine(urlDirPath, fileName);
        }

        public string GetGalleryFileSavePath(string fileName)
        {
            return Path.Combine(dirSavePath, fileName);
        }

        public string GetGalleryDirUrlPath()
        {
            var dir = Enviroment.GetUrlDirPath(fileName);
            return dir;
        }

        public string GetGalleryConfigUrlPath()
        {
            var dir = GetGalleryDirUrlPath();
            var path = Path.Combine(dir, GET_DIR_SLIDES_FILENAME);
            return path;
        }

        public string GetGalleryConfigSavePath()
        {
            return Path.Combine(Enviroment.GetSavePath(fileName), GALLERY_CONFIG_FILENAME);
        }

        public string GetGalleryDirSavePath()
        {
            return Enviroment.GetSavePath(fileName);
        }

        void SetPaths()
        {
            urlDirPath = GetGalleryDirUrlPath();
            urlConfigFilePath = GetGalleryConfigUrlPath();
            dirSavePath = GetGalleryDirSavePath();
            configSavePath = GetGalleryConfigSavePath();
        }

        public override async Task<(bool, string)> DownloadTask()
        {
            SetPaths();
            if (fileName == "")
            {
                return (true, "skipped:" + name);
            }
            FileDownloadStarted(urlConfigFilePath);

            UnityWebRequest www = UnityWebRequest.Get(urlConfigFilePath);
            bool resultOk = await HandleWWWRequest(www) != null;

            if (resultOk)
            {

               // Debug.Log("gal data=" + www.downloadHandler.text);
                Gallery galleryData = new Gallery();
                JsonUtility.FromJsonOverwrite(www.downloadHandler.text, galleryData);
                //string UrlDirPath = Enviroment.GetSavePath(relativeUrl);

                //Debug.Log("dirSavePath="+dirSavePath);
                //Debug.Log("configSavePath=" + configSavePath);
               // Debug.Log("UrlDirPath=" + UrlDirPath);
                //Directory.CreateDirectory(dirPath);
                /*
                Debug.Log("saving:");
                Debug.Log(www.downloadHandler.text);
                Debug.Log("to:");
                Debug.Log(savePath);

                Debug.Log("diropath");
                Debug.Log(dirPath);*/
                CreateDirsForFilePath(configSavePath);
                System.IO.File.WriteAllBytes(configSavePath, www.downloadHandler.data);
               // FileDownloaded(urlPath);
                //todo async

                Task<bool>[] tasks = new Task<bool>[galleryData.slides.Length];
                for (int i = 0; i < galleryData.slides.Length; i++)
                {
                    Slide slide = galleryData.slides[i];
                    //Debug.Log("subfile_url=" + GetGalleryFileUrlPath(slide.path));
                    //Debug.Log("subfile_path=" + GetGalleryFileSavePath(slide.path));
                    tasks[i] = DownloadSlideTask(GetGalleryFileUrlPath(slide.path), GetGalleryFileSavePath(slide.path));
                }
                await Task.WhenAll(tasks);
            }
            return (resultOk, urlConfigFilePath);
        }

        public async Task<bool> DownloadSlideTask(string slideUrlPath, string slideSavePath)
        {
            UnityWebRequest imageWWW = UnityWebRequestTexture.GetTexture(slideUrlPath);

            bool imageOk = await HandleWWWRequest(imageWWW) != null;
            if (imageOk)
            {
                Texture2D texture = DownloadHandlerTexture.GetContent(imageWWW);
                string imageSavePath = GetGalleryFileSavePath(slideSavePath);

                System.IO.File.WriteAllBytes(imageSavePath, imageWWW.downloadHandler.data);
                //FileDownloaded(imageSavePath);
            }
            return imageOk;
        }

        public async Task<Texture2D[]> LoadLocallyTask()
        {
            string savePath = GetGalleryConfigSavePath();
            //load gallery json
            UnityWebRequest www = UnityWebRequest.Get(savePath);
            bool resultOk = await HandleWWWRequest(www) != null;
            if (resultOk)
            {
                Gallery galleryData = new Gallery();
                JsonUtility.FromJsonOverwrite(www.downloadHandler.text, galleryData);
                //Debug.Log(JsonUtility.ToJson(galleryData));

                FileLoadedLocaly(savePath);
                //TODO doparalel
                Texture2D[] textures = new Texture2D[galleryData.slides.Length];
                for (int i = 0; i < galleryData.slides.Length; i++)
                {
                    string relativeImagePath = galleryData.slides[i].path;
                    var imagePath = GetGalleryFileSavePath(relativeImagePath);
                    UnityWebRequest imageWWW = UnityWebRequestTexture.GetTexture(imagePath);
                    bool imageOk = await HandleWWWRequest(imageWWW) != null;
                    if (imageOk)
                    {
                        Texture2D texture = DownloadHandlerTexture.GetContent(imageWWW);

                        textures[i] = texture;

                        FileLoadedLocaly(imagePath);
                    }
                    else
                    {
                        //wher error, return incomplete array
                        FileLog("incomplete galery: fail on"+ relativeImagePath);
                        return textures;
                    }

                }
                FileLog("galerry images loaded: "+ galleryData.slides.Length);
                return textures;
            }
            else
            {
                FileLog("empty galery");
                return GetMimimunContent();
            }
        }

        public override void DisposeContent()
        {
            throw new NotImplementedException();
        }
        /*
       public async Task<Texture2D> LoadSlideLocalyTask(string slideSavePath, int i)
       {
           UnityWebRequest imageWWW = UnityWebRequestTexture.GetTexture(slideSavePath);
           bool imageOk = await HandleWWWRequest(imageWWW) != null;
           if (imageOk)
           {
               Texture2D texture = DownloadHandlerTexture.GetContent(imageWWW);

               textures[i] = texture;

               FileLoadedLocaly(imagePath);
           }
           else
           {
               //wher error, return incomplete array
               return textures;
           }
       }*/

    }
}