﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using Sense3d.Fileloading.RemoteHandlers;
using UnityEngine.Networking;
using System.Threading.Tasks;
using Sense3d.Fileloading.Utils;

namespace Sense3d.Fileloading
{
    [CreateAssetMenu(fileName = "RemoteGameConfig", menuName = "RemoteFileHandlers/GameConfig")]
    public class RemoteGameConfigHandler : RemoteTextHandler
    {
        public const string REMOTE_FILENAME = "config.json";
        private void Awake()
        {
            fileName = REMOTE_FILENAME;
//            Enviroment.AppUrlConfig.urlPath
        }
    }
}
