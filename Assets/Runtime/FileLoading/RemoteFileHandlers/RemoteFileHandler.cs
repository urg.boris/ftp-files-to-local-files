﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.Networking;
using System.Threading.Tasks;
using Sense3d.Fileloading.Utils;
using UnityEditor;

namespace Sense3d.Fileloading.RemoteHandlers
{
    //public interface IRemoteFileHandler

    public abstract class RemoteFileHandler : ScriptableObject
    {
        const string FILE_PROTOCOL = "file://";
        [SerializeField]
        public string fileName;
        //[HideInInspector]
        public string relativeUrl;
        public abstract void LoadLocalContent();
        public abstract Task<bool> LoadLocalContentBlocking();
        public abstract void DisposeContent();

        public abstract Task<(bool, string)> DownloadTask();

        public abstract void DownloadRemoteFile();

        


        public void AddjustPath(string path)
        {
            //Debug.Log("adjusting to "+ path);
            relativeUrl = path;
        }

        public virtual bool ExistsFileLocally()
        {
            
            //Debug.Log(Enviroment.AppConfig);
            var filepath = GetSavePath();
            //Debug.Log("ee localy="+ filepath);
            return File.Exists(filepath);
        }

        public static void FileDownloadStarted(string path)
        {
            Debug.Log("starting downloading: file=" + path);
        }

        public static void FileLoadedLocaly(string path)
        {
            Debug.Log("<color=green>loaded localy: file=" + path + "</color>");
        }

        public void FileLoadedWithPlaceholder(string path)
        {
            /*
#if UNITY_EDITOR
            string assetPath = AssetDatabase.GetAssetPath(this.GetInstanceID());
            item.ItemIdentifiers.ItemName = Path.GetFileNameWithoutExtension(assetPath);
#endif*/
            Debug.Log("<color=yellow>"+name+"loaded with placeholder: file=" + path + "</color>");
        }

        public static void FileDownloaded(string path)
        {
            Debug.Log("<color=green>downloaded: file=" + path + "</color>");
        }

        public static void FileLog(string msg)
        {
            Debug.Log("<color=green>"+msg+"</color>");
        }

        public string GetUrlPath()
        {
            return Enviroment.GetUrlPath(relativeUrl);
        }

        public string GetSavePath()
        {
            return Enviroment.GetSavePath(relativeUrl);
        }

        public string GetSavePathWithProtocol()
        {
            return FILE_PROTOCOL + GetSavePath();
        }

        protected async Task<UnityWebRequest> HandleWWWRequest(UnityWebRequest www)
        {
            UnityWebRequestAsyncOperation request = www.SendWebRequest();
            while (!request.isDone || !www.isDone) await Task.Delay(30);

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.LogError($"WWW ({www.url}) has error: {www.error}");
                return null;
            }

            return www;
        }

        public static void CreateDirsForFilePath(string filePath)
        {
            //Debug.Log("creating dir=" + filePath);
            //create subdirs
            System.IO.FileInfo file = new System.IO.FileInfo(filePath);
            file.Directory.Create();
        }

    }
}
