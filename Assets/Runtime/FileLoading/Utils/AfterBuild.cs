using System.Diagnostics;
using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System;
#if UNITY_IOS
//using UnityEditor.iOS.Xcode;
#endif


#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEditor.Callbacks;
#endif
namespace Sense3d.Fileloading.Utils
{
    public class AfterBuild
    {
        /*
        [MenuItem("AfterBuild/Copy App Configs")]
        public static void GetFilesToCopy()
        {
            string path = EditorUtility.SaveFolderPanel("Choose Location of Built Game", "", "");

            //UnityEngine.Debug.Log(path);

            FileUtil.CopyFileOrDirectory("Assets/Configs", path + "/Configs");

            string[] filePaths = Directory.GetFiles(path + "/Configs", "*.meta", SearchOption.AllDirectories);

            int index = Application.dataPath.Length;
            for (int i = 0; i < filePaths.Length; i++)
            {
                FileUtil.DeleteFileOrDirectory(filePaths[i]);
            }

            UnityEngine.Debug.Log("app configs copied");
        }*/
    }
    /*
    class MyCustomBuildProcessor : IPostprocessBuild
    {
        public int callbackOrder { get { return 0; } }
        public void OnPostprocessBuild(BuildTarget target, string path)
        {
            File.Copy("sourceFilePath", "destinationFilePath");
        }
    }*/
/*
#if UNITY_EDITOR
    [PostProcessBuild]
    class PreBuild : IPostprocessBuildWithReport
    {
        public int callbackOrder { get { return 0; } }
        public void OnPostprocessBuild(BuildReport report)
        {
            string relativeDirName = "Configs/";
            string rootProjectFolder = Path.Combine(Application.dataPath, "../");
            string sourceFolder = Path.Combine(rootProjectFolder, relativeDirName);
            string rootBuildFolder;
            string targetFolder;

            if (report.summary.platform == BuildTarget.iOS)
            {
                targetFolder = Path.Combine(report.summary.outputPath, relativeDirName);

                //!!for itunes file sharing
                // Get plist
                string plistPath = report.summary.outputPath + "/Info.plist";
                PlistDocument plist = new PlistDocument();
                plist.ReadFromString(File.ReadAllText(plistPath));
                // Get root
                PlistElementDict rootDict = plist.root;
                rootDict.SetBoolean("UIFileSharingEnabled", true);
                // Write to file
                File.WriteAllText(plistPath, plist.WriteToString());

            
                //!!add configs
                string projPath = report.summary.outputPath + "/Unity-iPhone.xcodeproj/project.pbxproj";

                PBXProject proj = new PBXProject();
                proj.ReadFromString(File.ReadAllText(projPath));
                string target = proj.TargetGuidByName("Unity-iPhone");

                Directory.CreateDirectory(targetFolder);

                string[] filePaths = Directory.GetFiles(targetFolder, "*.json", SearchOption.AllDirectories);

                int index = Application.dataPath.Length;
                for (int i = 0; i < filePaths.Length; i++)
                {
                    var srcPath = Path.Combine(sourceFolder, filePaths[i]);
                    var dstLocalPath = relativeDirName + filePaths[i];
                    var dstPath = Path.Combine(report.summary.outputPath, dstLocalPath);
                    File.Copy(srcPath, dstPath, true);
                    proj.AddFileToBuild(target, proj.AddFile(dstLocalPath, dstLocalPath));
                }
                File.WriteAllText(projPath, proj.WriteToString());
                UnityEngine.Debug.Log("app configs copied");
                / *
                Directory.CreateDirectory(Path.Combine(report.summary.outputPath, "Libraries/Unity"));

                string[] filesToCopy = new string[]
                {
                    "PlatformBase.h",
                    "RenderAPI_Metal.mm",
                    "RenderAPI_OpenGLCoreES.cpp",
                    "RenderAPI.cpp",
                    "RenderAPI.h",
                    "RenderingPlugin.cpp",
                };

                for (int i = 0; i < filesToCopy.Length; ++i)
                {
                    var srcPath = Path.Combine("../PluginSource/source", filesToCopy[i]);
                    var dstLocalPath = "Libraries/" + filesToCopy[i];
                    var dstPath = Path.Combine(report.summary.outputPath, dstLocalPath);
                    File.Copy(srcPath, dstPath, true);
                    proj.AddFileToBuild(target, proj.AddFile(dstLocalPath, dstLocalPath));
                }

                File.WriteAllText(projPath, proj.WriteToString());* /
            }
            else
            {
                rootBuildFolder = Path.Combine(report.summary.outputPath, "../");
                targetFolder = Path.Combine(rootBuildFolder, relativeDirName);

                FileUtil.CopyFileOrDirectory(sourceFolder, targetFolder);

                string[] filePaths = Directory.GetFiles(targetFolder, "*.meta", SearchOption.AllDirectories);

                int index = Application.dataPath.Length;
                for (int i = 0; i < filePaths.Length; i++)
                {
                    FileUtil.DeleteFileOrDirectory(filePaths[i]);
                }

                UnityEngine.Debug.Log("app configs copied");
            }

        }



    }
    * /
    / *
    public static class PostBuild
    {

        // Automatically increment the version number for each unity build. Particularly useful so you can push Unity Cloud Build projects to prod with a peace of mind.
        [PostProcessBuild]
        public static void OnPostprocessBuild(BuildTarget buildTarget, string path)
        {
            if (PlayerSettings.bundleVersion != String.Format("{0:yyMMdd}", System.DateTime.Now))
            {
                throw new Exception("build verze nesedi s dneskem");
            }


        }

    }* /

    

#endif*/
}
