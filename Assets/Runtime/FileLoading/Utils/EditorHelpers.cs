using Sense3d.Fileloading;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Sense3d.Fileloading.Utils
{
    public class EditorHelpers
    {
#if UNITY_EDITOR
        public static void LoadAppConfig<T>(T appConfig) where T: ScriptableObject
        {
            string typeName = typeof(T).Name;
            var appConfigLoader = new JsonConfigFileHandler(Enviroment.GetConfigsPath());


            appConfigLoader.LoadConfig(appConfig, Enviroment.APP_CONFIG_FILENAME);
            //Debug.Log(JsonUtility.ToJson(appConfig));
            EditorUtility.SetDirty(appConfig);

            Enviroment.AppUrlConfig = appConfig as AppUrlConfig;

            Debug.Log(typeName + " loaded for editor");
        }
        /*
        public static void CreateAppConfigFile<T>() where T: ScriptableObject
        {
            var newConfig = ScriptableObject.CreateInstance<T>();
            var appConfigLoader = new JsonConfigFileHandler(Enviroment.GetConfigsPath());
            appConfigLoader.SaveConfig(newConfig, Enviroment.APP_CONFIG_FILENAME);

            Debug.Log(Enviroment.APP_CONFIG_FILENAME + " created");
        }*/
#endif
    }
}
