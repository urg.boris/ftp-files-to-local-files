using Sense3d.Fileloading;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Sense3d.Fileloading.Utils
{

    public static class Enviroment
    {
        public const string APP_CONFIG_FILENAME = "appConfig.json";

        private static AppUrlConfig _appUrlConfig;
        public static AppUrlConfig AppUrlConfig
        {
            get
            {
                if (_appUrlConfig == null)
                {
#if UNITY_EDITOR
                    EditorHelpers.LoadAppConfig(AppUrlConfig);
#else
                    //TODO??probably loaded by runtime
#endif
                }
                return _appUrlConfig;
            }
            set => _appUrlConfig = value;
        }

        static string GetUrlHash()
        {
            var hash = UnityEngine.Random.Range(0, 999999999);
/*
            var hash = new Hash128();
            Hash128.Compute(new DateTime().ToShortDateString());*/
            return hash.ToString();
        }
        
        public static string GetUrlPath(string relativeUrl)
        {
            //prevent cache
            return _appUrlConfig.urlPath + relativeUrl +"?"+ GetUrlHash();
        }

        public static string GetUrlDirPath(string relativeUrl)
        {
            //prevent cache
            return _appUrlConfig.urlPath + relativeUrl;
        }

        public static string GetSavePath(string relativePath)
        {
            //Debug.Log(Application.persistentDataPath);
            //Debug.Log(Path.Combine(Application.persistentDataPath, relativePath));
            return Path.Combine(Application.persistentDataPath, relativePath);
        }
        
        public static string GetAppConfigPath()
        {
            return GetConfigsPath() + APP_CONFIG_FILENAME;
        }


        public static string GetConfigsPath()
        {
#if !UNITY_EDITOR
        return  Application.streamingAssetsPath + "/Configs/" ;
#endif
#if UNITY_EDITOR
            return Application.streamingAssetsPath + "/Configs/";
#endif
#if UNITY_IOS
            return Application.streamingAssetsPath + "/Configs/";
#endif
        }
    }
}
