using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Sense3d.Fileloading.UI
{
    [AddComponentMenu("RemoteFileUI/RemoteTextUI")]
    public class RemoteTextUI : Text
    {
        public Text textField;
        public RemoteTextHandler remoteFileHandler;

        protected void Awake()
        {
#if !UNITY_EDITOR
            remoteFileHandler.LoadLocalContent();
#endif
        }
    }


}
