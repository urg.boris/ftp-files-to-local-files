using Sense3d.Fileloading.RemoteHandlers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Sense3d.Fileloading.UI
{
        [AddComponentMenu("RemoteFileUI/RemoteAudioUI")]
    public class RemoteAudioUI : MonoBehaviour
    {
        public AudioSource audioSource;
        public RemoteAudioHandler remoteFileHandler;

        private void Awake()
        {
#if !UNITY_EDITOR
            remoteFileHandler.LoadLocalContent();
#endif
        }
    }

    
}
